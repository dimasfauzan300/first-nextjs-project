import { BussLogicFile } from "@/app/bussLogic/FileBussLogic";
import { UserSessionBussLogic } from "@/app/bussLogic/UserSessionBussLogic";
import { createUploadthing, type FileRouter } from "uploadthing/next";

const f = createUploadthing();

export const ourFileRouter = {
  pdfUploader: f({ pdf: { maxFileSize: "4MB" } })
    .middleware(async ({ req }) => {
      const user = await UserSessionBussLogic.getUserSession();

      if (!user || !user.id) throw new Error("Unauthorized");

      return { userId: user.id };
    })
    .onUploadComplete(async ({ metadata, file }) => {
      const createFile = await BussLogicFile.createFile({
        key: file.key,
        name: file.name,
        userId: metadata.userId,
        url: `https://uploadthing-prod.s3.us-west-2.amazonaws.com/${file.key}`,
        uploadStatus: "PROCESSING",
      });
    }),
} satisfies FileRouter;

export type OurFileRouter = typeof ourFileRouter;
