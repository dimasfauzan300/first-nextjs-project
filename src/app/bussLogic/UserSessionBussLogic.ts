import { KindeUser } from "@kinde-oss/kinde-auth-nextjs/dist/types";
import { getKindeServerSession } from "@kinde-oss/kinde-auth-nextjs/server";

async function getUserSession() {
  const { getUser } = getKindeServerSession();
  const user: KindeUser | null = await getUser();
  return user;
}

export const UserSessionBussLogic = { getUserSession };
