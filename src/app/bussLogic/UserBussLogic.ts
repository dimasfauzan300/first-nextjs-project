import { db } from "@/db";
import { Prisma } from "@prisma/client";

async function getUserById(userId: string) {
  const dbUser = await db.user.findFirst({ where: { id: userId } });
  return dbUser;
}

async function createUser(data: Prisma.UserCreateInput) {
  await db.user.create({
    data: data,
  });
}

export const UserBussLogic = { getUserById, createUser };
