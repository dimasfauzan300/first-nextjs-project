import { db } from "@/db";
import { File, Prisma } from "@prisma/client";

async function getFileById(fileId: string, userId: string) {
  const file = await db.file.findFirst({
    where: { id: fileId, userId: userId },
  });
  return file;
}

async function deleteFileById(fileId: string) {
  await db.file.delete({ where: { id: fileId } });
}

async function getAllFileByIdUser(userId: string) {
  const file = await db.file.findMany({
    where: { userId: userId },
  });
  return file;
}

async function createFile(data: Prisma.FileCreateManyInput) {
  return await db.file.create({
    data,
  });
}

async function checkFileIsExist(key: string, userId: string) {
  return await db.file.findFirst({ where: { key: key, userId: userId } });
}

export const BussLogicFile = {
  getFileById,
  deleteFileById,
  getAllFileByIdUser,
  createFile,
  checkFileIsExist,
};
