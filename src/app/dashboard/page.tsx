// "use client";
import Dashboard from "@/components/Dashboard";
import { redirect } from "next/navigation";
import { UserSessionBussLogic } from "@/app/bussLogic/UserSessionBussLogic";
import { UserBussLogic } from "../bussLogic/UserBussLogic";
// import React, { useState } from "react";
async function Page() {
  const user = await UserSessionBussLogic.getUserSession();
  if (!user || !user.id) redirect("/auth-callback?origin=dashboard");

  const dbUser = await UserBussLogic.getUserById(user.id);

  if (!dbUser) redirect("/auth-callback?origin=dashboard");

  return <Dashboard />;
}

export default Page;
