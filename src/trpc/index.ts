import { privateProcedure, publicProcedure, router } from "./trpc";
import { TRPCError } from "@trpc/server";
import { z } from "zod";
import { UserSessionBussLogic } from "@/app/bussLogic/UserSessionBussLogic";
import { BussLogicFile } from "@/app/bussLogic/FileBussLogic";
import { UserBussLogic } from "@/app/bussLogic/UserBussLogic";

export const appRouter = router({
  authCallBack: publicProcedure.query(async () => {
    const user = await UserSessionBussLogic.getUserSession();
    if (!user || !user.id || !user.email) {
      throw new TRPCError({ code: "UNAUTHORIZED" });
    }
    //check if user in database
    const dbUser = await UserBussLogic.getUserById(user.id);
    if (!dbUser) {
      await UserBussLogic.createUser({ email: user.email, id: user.id });
    }
    return { email: user.email, given_name: user.given_name };
  }),

  getUserFiles: privateProcedure.query(async (ctx) => {
    const { user } = ctx.ctx;
    return await BussLogicFile.getAllFileByIdUser(user.id);
  }),

  deleteFile: privateProcedure
    .input(z.object({ id: z.string() }))
    .mutation(async ({ ctx, input }) => {
      const { userId } = ctx;
      const file = await BussLogicFile.getFileById(input.id, userId);
      if (!file) throw new TRPCError({ code: "NOT_FOUND" });
      await BussLogicFile.deleteFileById(file.id);
      return file;
    }),

  getSession: privateProcedure.query(async () => {
    const user = await UserSessionBussLogic.getUserSession();
    if (!user || !user.id || !user.email) {
      throw new TRPCError({ code: "UNAUTHORIZED" });
    }
    const dbUser = await UserBussLogic.getUserById(user.id);
    return dbUser;
  }),

  getFile: privateProcedure
    .input(z.object({ key: z.string() }))
    .mutation(({ ctx, input }) => {
      const { userId } = ctx;
      const file = BussLogicFile.checkFileIsExist(input.key, userId);
      if (!file) throw new TRPCError({ code: "NOT_FOUND" });
      return file;
    }),
});

export type AppRouter = typeof appRouter;
