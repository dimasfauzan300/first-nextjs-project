import { TRPCError, initTRPC } from "@trpc/server";
import { UserSessionBussLogic } from "@/app/bussLogic/UserSessionBussLogic";

const t = initTRPC.create();
const middleware = t.middleware;
const isAuth = middleware(async (opts) => {
  const user = await UserSessionBussLogic.getUserSession();
  if (!user || !user.id || !user.email) {
    throw new TRPCError({ code: "UNAUTHORIZED" });
  }

  return opts.next({
    ctx: {
      userId: user.id,
      user,
    },
  });
});

export const router = t.router;
export const publicProcedure = t.procedure;
export const privateProcedure = t.procedure.use(isAuth);
